<?php
//Function to calculate the depth of the array 
function Arraydepth(array $Arraydata)
{
    $depth = 1;
    foreach ($Arraydata as $val) 
    {
      //is_array is a standard function to determine whether its an array or not
        if (is_array($val)) 
        {
          //Recursive function call
            $depth += Arraydepth($val);
            break;
        }
    }

    return $depth;
}

//sample array to be passed to the function as a parameter
$Arraydata = array( 
            "person" => array (
               "anand" => array(
                "age" =>    array(
                  "dob" =>   "12-08-1991" 
                  )
                )
              )
         );

//call to find the depth of the array
  $result = Arraydepth($Arraydata);

//Prints the result
  echo $result;

?>
