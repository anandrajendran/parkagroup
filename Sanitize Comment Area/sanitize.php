
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
  //server side validation
  if(strlen($_POST["comment"]) == 0)
  {
    $msg = "Input atleast one character";

    header("Location:form.php?msg=$msg");
  }
  
  $comment = filter_input(INPUT_POST, 'comment', FILTER_VALIDATE_URL);
  
  //Function to sanitize the input
  function SanitizeInputs($comment)
  {
    //standard function to eliminate other html tags except anchor, bold, ilatics and image tags
    $output = htmlentities ( strip_tags($comment, '<a><b><i><img>') , ENT_NOQUOTES ); 
    
    return $output;
  }

  //Function to avoid/prevent sqlinjections
  function SqlInjection($comment)
  {
     // Eliminate the keywords like Insert,  update, drop, and union from being added to the database
    //return the clean data
    
    return cleandata;
  }

  function XSSScripting($comment)
  {
    //htmlspecialchars function identifies any input you do not want to be HTML output and converts it into plain HTML entities. 
    //For example: ‘&’ becomes ‘&’ and ‘”‘ (double quote) becomes ‘"’;

    return cleandataa;
  }

  //Call the sanitize function
  $result = SanitizeInputs($_POST["comment"]);

  //Call the SqlInjection function
  $SqlInjection = SqlInjection($_POST["comment"]);

  //Call the SqlInjection function
  $XSSScripting = XSSScripting($_POST["comment"]);

  echo "The input was". $_POST["comment"];
  echo "<br>";
  echo "The sanitized output is ". $result;
  exit;



  //An alternate implemention using HTMLPURIFIERS
  //function SanitizeInputs($comment)
  //{
  //require_once('htmlpurifier/library/HTMLPurifier.auto.php');
  //$config = HTMLPurifier_Config::createDefault();
  //$config->set('Core', 'Encoding', 'ISO-8859-1'); 
  //$config->set('HTML', 'Allowed', '<a><b><i><img>'); // Allow anchor, bold, ilatics and image tags
  //$purifier = new HTMLPurifier($config);
 // return $purifier->purify($comment);
  //}  

  
  
  
}
?>



