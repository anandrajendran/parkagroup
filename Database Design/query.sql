
/*created 3 table*/
CREATE TABLE category
(
category_id int NOT NULL PRIMARY KEY,
category_name varchar(255)
)

CREATE TABLE itemtype
(
itemtype_id int NOT NULL PRIMARY KEY,
itemtype_name varchar(255)
)

CREATE TABLE item
(
item_id int NOT NULL PRIMARY KEY,
item_name varchar(255),
price varchar(255),
category_id int,
itemtype_id int,
FOREIGN KEY (category_id) REFERENCES category(category_id),
FOREIGN KEY (itemtype_id) REFERENCES itemtype(itemtype_id)
)


/*To get all items from category 'X' that is electronics*/
SELECT  `item_name` as `Items from electronics`
FROM category
INNER JOIN item
ON category.category_id=item.category_id
WHERE category.category_id = 2


/*To get all items and sub-categories from category 'X' that is electronics.*/
SELECT  `category_name` as `CATEGORY`, `itemtype_name` as `SUB CATEGORY`, `item_name` as `ITEMs` 
FROM category
INNER JOIN item
ON category.category_id=item.category_id
INNER JOIN itemtype
ON itemtype.itemtype_id =item.itemtype_id
WHERE category.category_id = 2
